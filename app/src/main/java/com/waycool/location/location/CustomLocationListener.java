package com.waycool.location.location;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

/**
 * Created by KarthickKumar on 28-01-2019.
 */

public class CustomLocationListener implements LocationListener, NumberConstants {
    protected LocationManager locationManager;
    private static CustomLocationListener ourInstance = new CustomLocationListener();
    private Context context;

    public static CustomLocationListener getInstance() {
        return ourInstance;
    }


    /*
    * Start location listener
    */
    public void startListen(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        this.context = context;
        if (locationManager == null) {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
    }

    /*
    * Stop location listener
    */
    public void stopListen(Context context) {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (locationManager != null) {
            locationManager.removeUpdates(this);
            locationManager = null;
        }
    }

    /*
    * Set lat, long to singleton variable
    */
    @Override
    public void onLocationChanged(Location location) {
        LocationDetails.getInstance().setLocationStatus(NumberConstants.locationStatusEnabled);
        LocationDetails.getInstance().setLatitude(location.getLatitude());
        LocationDetails.getInstance().setLongitude(location.getLongitude());
        notifyActivity();
    }

    /*
    * Set error code for location disable to singleton variable
    */
    @Override
    public void onProviderDisabled(String provider) {
        LocationDetails.getInstance().setLocationStatus(NumberConstants.locationStatusDisabled);
        LocationDetails.getInstance().setLatitude(0);
        LocationDetails.getInstance().setLongitude(0);
        notifyActivity();
    }

    /*
    * Set code for location enabled to singleton variable
    */
    @Override
    public void onProviderEnabled(String provider) {
        LocationDetails.getInstance().setLocationStatus(NumberConstants.locationStatusEnabled);
        notifyActivity();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    /*
    * Notify to activity
    */
    private void notifyActivity() {
        Intent intent = new Intent(context, ActivityLocation.class);
        NotifyObservable.getInstance().updateValue(intent);
    }
}
