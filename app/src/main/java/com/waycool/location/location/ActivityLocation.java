package com.waycool.location.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

public class ActivityLocation extends AppCompatActivity implements Observer {

    private int PERMISSION_ALL = NumberConstants.constantOne;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION};
    private Typeface fontStyle;
    private TextView textViewErrorMessage, textViewLoading, textviewLat, textViewLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_location);
        assignObjects();
        setFont();
        if (!checkAppPermissions()) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }

    /*
    * Add observer
    */
    protected void onResume() {
        super.onResume();
        NotifyObservable.getInstance().addObserver(this);
        if (checkAppPermissions()) {
            CustomLocationListener.getInstance().startListen(this);
            textViewErrorMessage.setVisibility(View.GONE);
            textViewLoading.setVisibility(View.VISIBLE);
        } else {
            textviewLat.setText(getString(R.string.empty_string));
            textViewLong.setText(getString(R.string.empty_string));
        }
    }

    /*
    * Delete observer
    */
    protected void onPause() {
        super.onResume();
        NotifyObservable.getInstance().deleteObserver(this);
        CustomLocationListener.getInstance().stopListen(this);
    }

    /*
    * Stop location listener
    */
    protected void onDestroy() {
        super.onDestroy();
        CustomLocationListener.getInstance().stopListen(this);
    }

    /*
    * Assign objects
    */
    private void assignObjects() {
        textViewErrorMessage = (TextView) findViewById(R.id.textview_error_message);
        textviewLat = (TextView) findViewById(R.id.textview_lat);
        textViewLong = (TextView) findViewById(R.id.textview_long);
        textViewLoading = (TextView) findViewById(R.id.textview_loading_message);
        fontStyle = Typeface.createFromAsset(getApplicationContext().getAssets(),
                getString(R.string.font_poppins_sb));
    }

    /*
    * Set font style
    */
    private void setFont() {

        if (textViewLoading != null && fontStyle != null) {
            textViewLoading.setTypeface(fontStyle);
        }
        if (textViewErrorMessage != null && fontStyle != null) {
            textViewErrorMessage.setTypeface(fontStyle);
        }
        if (textViewLong != null && fontStyle != null) {
            textViewLong.setTypeface(fontStyle);
        }
        if (textviewLat != null && fontStyle != null) {
            textviewLat.setTypeface(fontStyle);
        }
    }

    /*
    * Update lat, long value or error message
     */
    @Override
    public void update(Observable o, Object arg) {

        if (LocationDetails.getInstance().getLocationStatus() == NumberConstants.locationStatusDisabled) {
            textViewErrorMessage.setText(getString(R.string.location_on));
            textviewLat.setText(getString(R.string.empty_string));
            textViewLong.setText(getString(R.string.empty_string));
            textViewErrorMessage.setVisibility(View.VISIBLE);
        } else {
            textViewErrorMessage.setVisibility(View.GONE);
            if (LocationDetails.getInstance().getLatitude() > 0 && LocationDetails.getInstance().getLongitude() > 0) {
                textviewLat.setText(getString(R.string.location_lat) + LocationDetails.getInstance().getLatitude());
                textViewLong.setText(getString(R.string.location_long) + LocationDetails.getInstance().getLongitude());
                textViewLoading.setVisibility(View.GONE);
            } else {
                textViewLoading.setVisibility(View.VISIBLE);
            }

        }
    }

    /*
    * Check the permission if not granted show the dialog
     */
    private boolean checkAppPermissions() {
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            return false;
        }
        //If permission is not granted returning false
        return true;
    }

    /*
    * Check the permission
    * */
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {

                    return false;
                }
            }
        }
        return true;
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == PERMISSION_ALL) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CustomLocationListener.getInstance().startListen(this);
                textViewErrorMessage.setVisibility(View.GONE);
                textViewLoading.setVisibility(View.VISIBLE);
            } else {
                textViewErrorMessage.setText(getString(R.string.location_permission_on));
                textViewErrorMessage.setVisibility(View.VISIBLE);
//                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }
}
