package com.waycool.location.location;

/**
 * Created by KarthickKumar on 28-01-2019.
 */
public class LocationDetails {
    private static LocationDetails ourInstance = new LocationDetails();

    private int locationStatus;
    private double latitude, longitude;
    public static LocationDetails getInstance() {
        return ourInstance;
    }

    private LocationDetails() {
    }

    public int getLocationStatus() {
        return locationStatus;
    }

    public void setLocationStatus(int locationStatus) {
        this.locationStatus = locationStatus;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
